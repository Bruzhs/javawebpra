package javac.bookstore;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;



public class BookDAO {
	private String jdbcURL ="jdbc:mysql://localhost:3306/Bookstore";
    private String jdbcUsername = "root";
    private String jdbcPassword = "1234";
        
    private String insertSql = "INSERT INTO book (title, author, price) VALUES (?, ?, ?)";
    private String listAllBoolsql = "SELECT * FROM book";
    private String deleteSql = "DELETE FROM book where book_id = ?";
    private String updateSql = "UPDATE book SET title = ?, author = ?, price = ? WHERE book_id = ?";
    private String SelecdtOneSql = "SELECT * FROM book WHERE book_id = ?";
	/**
	 * @param jdbcURL
	 * @param jdbcUsername
	 * @param jdbcPassword
	 */
//	public BookDAO(String jdbcURL, String jdbcUsername, String jdbcPassword) {
//		super();
//		this.jdbcURL = jdbcURL;
//		this.jdbcUsername = jdbcUsername;
//		this.jdbcPassword = jdbcPassword;
//	}
    
	protected Connection connect()  {
    	Connection jdbcConnection = null;
        try {
                Class.forName("com.mysql.jdbc.Driver");
                jdbcConnection = DriverManager.getConnection(jdbcURL, jdbcUsername, jdbcPassword);
        }catch(SQLException e) {
        	e.printStackTrace();
        }catch (ClassNotFoundException e) {
        	e.printStackTrace();
        }
        return jdbcConnection;
    }
	 public void insertBook(Book book) throws SQLException{
	    	try(Connection connection=connect();
	    			PreparedStatement preparedStatment = connection.prepareStatement(insertSql)){
	    		preparedStatment.setString(1,book.getTitle());
	    		preparedStatment.setString(2,book.getAuthor());
	    		preparedStatment.setFloat(3,book.getPrice());
	    		preparedStatment.executeUpdate();
	    	}catch(Exception e) {
	    		e.printStackTrace();
	    	}
	 }
	 public boolean updateBooks(Book book)throws SQLException{
	    	boolean rowUpdated;
	    	try(Connection connection = connect();
	    			PreparedStatement preparedStatment = connection.prepareStatement(updateSql)){
	    		preparedStatment.setString(1, book.getTitle());
	    		preparedStatment.setString(2,book.getAuthor());
	    		preparedStatment.setFloat(3,book.getPrice());
	    		preparedStatment.setInt(4,book.getId());
	    		preparedStatment.executeUpdate();
	    		
	    		rowUpdated =preparedStatment.executeUpdate() > 0;
	    	}
	    	return rowUpdated;
	 }
	 public Book getBook(int id){
	        Book book = null;
	        try(Connection connection = connect();
	        		PreparedStatement preparedStatement = connection.prepareStatement(SelecdtOneSql);){
	        preparedStatement.setInt(1, id);
	        
	        ResultSet rs= preparedStatement.executeQuery();
	        
	        while (rs.next()) {
	        	String title = rs.getString("title");
	        	String author = rs.getString("author");
	            float price = rs.getFloat("price");
	            book = new Book(id,title,author,price);
	        }
	        }catch (SQLException e) {
	        	e.printStackTrace();
	        }
	        return book;
	 }
	 public List<Book> listAllBook(){
	        List<Book> books = new ArrayList<Book>();
	        try(Connection connection = connect();
	        		PreparedStatement preparedStatement = connection.prepareStatement(listAllBoolsql);){
	       ResultSet rs= preparedStatement.executeQuery();
	        
	        while (rs.next()) {
	        	int id = rs.getInt("book_id");
	        	String title = rs.getString("title");
	        	String author = rs.getString("author");
	            float price = rs.getFloat("price");
	            books.add(new Book(id,title,author,price));
	        }
	        }catch (SQLException e) {
	        	e.printStackTrace();
	        }
	        return books;
	 }
	 public boolean deleteBook(int id) throws SQLException{
	    	boolean rowDel;
	    	try(Connection connection = connect();
	    			PreparedStatement preparedStatement = connection.prepareStatement(deleteSql);){
	    		preparedStatement.setInt(1,id );
	    		rowDel = preparedStatement.executeUpdate()>0;
	    	}
	    	return rowDel;
	    	
	    }
}


